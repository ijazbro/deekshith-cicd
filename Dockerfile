FROM tomcat:7.0.88
WORKDIR webapps 
COPY target/WebApp.war .
RUN rm -rf ROOT && mv WebApp.war ROOT.war
CMD ["catalina.sh", "run"]
